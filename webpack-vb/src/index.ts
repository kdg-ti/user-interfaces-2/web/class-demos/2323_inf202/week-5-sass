import 'bootstrap/dist/css/bootstrap.css'

import './scss/style.scss'

import {greeting} from './js/greeting'

document.getElementsByTagName('h1')[0].addEventListener('click', () => {
    console.log('Hello, H1!')
    greeting()
})

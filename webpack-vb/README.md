# About

## Usage

- From the project directory enter the following commands:

```
$ npm i
$ npm start
```

- Open your browser's development tools (usually F12).
- Click the header text and at the same time check the console.

## Technologies used

- Sass (scss)
- webpack
  - loaders for scss, css, ttf, etc.
- npm
- Bootstrap (+ jQuery en popper.js)
